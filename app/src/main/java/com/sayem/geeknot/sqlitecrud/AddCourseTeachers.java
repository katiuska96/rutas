package com.sayem.geeknot.sqlitecrud;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCourseTeachers extends AppCompatActivity {

    private Button btnStore;
    private EditText etcourse, etphone, ettiempo ;

    private DatabaseHelperTeacher databaseHelperTeacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course_teachers);

        databaseHelperTeacher = new DatabaseHelperTeacher(this);

        btnStore = (Button) findViewById(R.id.btnstore);
        final EditText etname = (EditText) findViewById(R.id.et_name);
        etcourse = (EditText) findViewById(R.id.et_course);
        final EditText etemail = (EditText) findViewById(R.id.et_email);
        etphone = (EditText) findViewById(R.id.et_phone);
        ettiempo = (EditText) findViewById(R.id.et_tiempo);


        btnStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = etname.getText().toString();
                if (TextUtils.isEmpty(name)){
                    etname.setError("Complete el campo");
                    etname.requestFocus();
                    return;
                }

                        String name2 = etcourse.getText().toString();
                    if (TextUtils.isEmpty(name2)){
                    etcourse.setError("Complete el campo");
                    etcourse.requestFocus();
                        return ;
                    }
                String name4 = etemail.getText().toString();
                if (TextUtils.isEmpty(name4)){
                    etemail.setError("Complete el campo");
                    etemail.requestFocus();
                    return ;

                }
                String name1 = etphone.getText().toString();
                if (TextUtils.isEmpty(name1)){
                    etphone.setError("Complete el campo");
                    etphone.requestFocus();
                    return;
                }

                String name3 = ettiempo.getText().toString();
                if (TextUtils.isEmpty(name3)){
                    ettiempo.setError("Complete el campo");
                    ettiempo.requestFocus();
                    return ;
                }



                databaseHelperTeacher.addTeachersDetail(

                        etphone.getText().toString(),
                        etcourse.getText().toString(),
                        etname.getText().toString(),


                    ettiempo.getText().toString(),

                        etemail.getText().toString(),

                etphone.getText().toString());

                ettiempo.getText().toString();
                etcourse.setText("");
                etphone.setText("");

                Toast.makeText(AddCourseTeachers.this, "DATOS AGREGADOS!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AddCourseTeachers.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }
}