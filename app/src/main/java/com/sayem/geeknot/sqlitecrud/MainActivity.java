package com.sayem.geeknot.sqlitecrud;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends Activity {

    Button agregarrutas;
    Button btn_lista, btn_salir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        agregarrutas= (Button)findViewById(R.id.btn_agregarrutas);
        agregarrutas.setOnClickListener(new View.OnClickListener() {
            @Override


            public void onClick(View v) {
                Intent agregarrutas = new Intent(MainActivity.this, AddCourseTeachers.class);
                startActivity(agregarrutas);

            }
        });
        btn_lista= (Button)findViewById(R.id.btn_lista);
        btn_lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(MainActivity.this, ModTeacher.class);
                startActivity(lista);

            }
        });

        btn_salir= (Button)findViewById(R.id.btnsalir);
        btn_salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(MainActivity.this, login1.class);
                startActivity(lista);

            }
        });


    }
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
    }

