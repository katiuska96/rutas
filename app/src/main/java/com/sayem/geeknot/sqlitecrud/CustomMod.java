package com.sayem.geeknot.sqlitecrud;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class CustomMod extends BaseAdapter {

    private Context context;
    private ArrayList<TeachersModel> teachersModelArrayList;

    public CustomMod(Context context, ArrayList<TeachersModel> teachersModelArrayList) {

        this.context = context;
        this.teachersModelArrayList = teachersModelArrayList;
    }


    @Override
    public int getCount() {
        return teachersModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return teachersModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.model_teacher_mod, null, true);


            holder.tvcourse = (TextView) convertView.findViewById(R.id.teachers_course);
            holder.tvname = (TextView) convertView.findViewById(R.id.teachers_name);
            holder.tvphone = (TextView) convertView.findViewById(R.id.teachers_phone);
            holder.tvtiempo = (TextView) convertView.findViewById(R.id.teachers_tiempo);
            holder.tvemail = (TextView) convertView.findViewById(R.id.teachers_email);
            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }
int c= 1;

        holder.tvcourse.setText("Ruta: "+teachersModelArrayList.get(position).getCourse());
        holder.tvname.setText("Origen: "+teachersModelArrayList.get(position).getName());
        holder.tvphone.setText("Destino: "+teachersModelArrayList.get(position).getPhone());
        holder.tvtiempo.setText("Compañias:"+teachersModelArrayList.get(position).getTiempo());
        holder.tvemail.setText("Valor pasaje: "+ c +teachersModelArrayList.get(position).getEmail());


        return convertView;
    }

    private class ViewHolder {

        protected TextView tvname, tvcourse, tvemail, tvphone, tvtiempo;
    }

}