package com.sayem.geeknot.sqlitecrud;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ModifyTeachersActivity extends AppCompatActivity {

    private TeachersModel teachersModel;
    private TextView etname, etcourse, etemail, etphone, ettiempo;
    private Button btnupdate, btndelete;
    private DatabaseHelperTeacher databaseHelperTeacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_teachers);

        Intent intent = getIntent();
        teachersModel = (TeachersModel) intent.getSerializableExtra("teachers");

        databaseHelperTeacher = new DatabaseHelperTeacher(this);

        etname = (TextView) findViewById(R.id.etname);
        etcourse = (TextView) findViewById(R.id.etcourse);
        etemail = (TextView) findViewById(R.id.etemail);
        etphone = (TextView) findViewById(R.id.etphone);
        ettiempo = (TextView) findViewById(R.id.ettiempo);
        btndelete = (Button) findViewById(R.id.btndelete);
        btnupdate = (Button) findViewById(R.id.btnupdate);

        etname.setText(teachersModel.getCourse());
        etcourse.setText(teachersModel.getName());
        etemail.setText(teachersModel.getPhone());
        etphone.setText(teachersModel.getTiempo());
        ettiempo.setText(1+teachersModel.getEmail());

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelperTeacher.updateTeachers(teachersModel.getId(),etname.getText().toString(),etcourse.getText().toString(),etemail.getText().toString(), etphone.getText().toString(), ettiempo.getText().toString());
                Toast.makeText(ModifyTeachersActivity.this, "Actualizacion correcta!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ModifyTeachersActivity.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelperTeacher.deleteUSer(teachersModel.getId());
                Toast.makeText(ModifyTeachersActivity.this, "Eliminacion correcta!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ModifyTeachersActivity.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }
}
